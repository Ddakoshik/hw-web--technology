<?php
include ("functions.php") ;
$connection = connect();
$productId = isset($_GET["product_id"]) ? $_GET["product_id"] : 1;
$product = getProductById($connection, $productId) ;
$categories = getCategories($connection) ;
$category = getCategoryById($connection, $product[0]['category_id']) ;
$productCat = $product[0]['category_id'];

$breadcrumb = [
  ["text" => "Головна", "link" => "index.php"],
  ["text" => $category['name'], "link" => "category.php?category_id=$productCat"],
  ["text" => $product[0]['name'], "link" => ""],
];
?>

<?php include ("header.php"); ?>

<main class="pb-5">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 col-lg-3">
        <div class="list-group">
          <div class="list-group-item list-group-item-dark">Категорії</div>
          <?php foreach ($categories as $categoryItem):?>
          <a class="list-group-item <?php if ($categoryItem['category_id'] == $productCat): ?>list-group-item-primary<?php endif; ?>" href="category.php?category_id=<?= $categoryItem['category_id'] ?>">
            <?= $categoryItem['name']; ?>
          </a>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-9">
        <h1><?= $product[0]['name']; ?> </h1>
        <p><?= $product['description']; ?></p>
        <div class="row">
          <div class="col-12 col-md-12">
            <div class="card p-1">
              <img class="card-img-top " src="/images/<?= $product[0]['image']; ?>" alt="Card img cap">
              <p><?= $product[0]['description']; ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>

<?php include("footer.php"); ?>