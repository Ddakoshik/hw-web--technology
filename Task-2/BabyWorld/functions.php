<?php

function connect() {
    //параметри зєднання
    $host = "localhost";
    $dbName = "babystore";
    $user = "root";
    $password = "";
    $dsn = "mysql:host=$host;dbname=$dbName;charset=utf8";
    //Зєднання з БД
    try {
        $connection = new PDO($dsn, $user, $password);
    } catch (Exception $e) {
        print_r("Error connection from database " . $dbName);die();
    }
    //Дані отримувати як асоціативні масиви
    $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    //Передаємо зєднання в наш код
    return $connection;
}

function getCategories(PDO $connection) {
    //формулюю sql інструкцію
    $sql = "SELECT * FROM category";
    //готую запит до виконання
    $stmt = $connection->prepare($sql);
    //виконую запита
    $stmt->execute();
    //Отримую масив результатів запиту та повертаємо в наш код
    return $stmt->fetchAll();
}

// function getCategoryById (PDO $connection, $categoryId) {
//     $sql = "SELECT * FROM category WHERE ($category_id=?) ";
// }

function getCategoryById(PDO $connection, $categoryId) {
    $sql = "SELECT * FROM category WHERE (category_id=?)" ;
    $stmt = $connection->prepare($sql) ;
    $stmt->execute([$categoryId]) ;
    return $stmt->fetch() ;
}

function getProductsByCategory(PDO $connection, $categoryId = 0) {
    $sql = "SELECT * FROM product WHERE (category_id=?)";
    $stmt = $connection->prepare($sql);
    $stmt->execute([$categoryId]);
    return $stmt->fetchAll();
}

function getProductById(PDO $connection, $productId = 0) {
    $sql = "SELECT * FROM product WHERE (product_id=?)";
    $stmt = $connection->prepare($sql);
    $stmt->execute([$productId]);
    return $stmt->fetchAll();
}