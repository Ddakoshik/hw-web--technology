<?php
include ("functions.php") ;
$connection = connect();
$categoryId = isset($_GET["category_id"]) ? $_GET["category_id"] : 1;
$categories = getCategories($connection) ;
$category = getCategoryById($connection, $categoryId) ;
$products = getProductsByCategory($connection, $categoryId) ;
$breadcrumb = [
  ["text" => "Головна", "link" => "index.php"],
  ["text" => $category['name'], "link" => ""],
];
?>

<?php include ("header.php"); ?>

<main class="pb-5">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 col-lg-3">
        <div class="list-group">
          <div class="list-group-item list-group-item-dark">Категорії</div>
          <?php foreach ($categories as $categoryItem):?>
          <a class="list-group-item <?php if ($categoryItem['category_id'] == $categoryId): ?>list-group-item-primary<?php endif; ?>" href="category.php?category_id=<?= $categoryItem['category_id'] ?>">
            <?= $categoryItem['name']; ?>
          </a>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-9">
        <h1><?= $category['name']; ?> </h1>
        <p><?= $category['description']; ?></p>
        <div class="row">
          <?php foreach($products as $productItem): ?>
          <div class="col-6 col-md-4">
            <div class="card p-1">
              <img class="card-img-top" src="/images/<?= $productItem['image']; ?>" alt="Card img cap">
              <div class="card-body">
                <h5 class="card-title">
                  <a href="product.php?product_id=<?= $productItem['product_id']; ?>">
                    <?= $productItem['name'] ?>
                  </a>
                </h5>
                <p class="card-text"><?= $productItem['short_description']; ?></p>
                <a href="#" class="btn btn-primary">Додати до кошика</a>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>

<?php include("footer.php"); ?>












































